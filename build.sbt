name := """small-loan"""
organization := "com.hfax"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

resolvers += "jitpack" at "https://jitpack.io"

scalaVersion := "2.11.8"

libraryDependencies += filters

libraryDependencies += javaJpa

libraryDependencies += "org.hibernate" % "hibernate-core" % "5.2.8.Final"

libraryDependencies += "mysql" % "mysql-connector-java" % "6.0.5"

libraryDependencies += "com.fasterxml.jackson.datatype" % "jackson-datatype-hibernate5" % "2.8.7"

libraryDependencies += "org.mindrot" % "jbcrypt" % "0.4"

libraryDependencies += "com.github.kenglxn.qrgen" % "javase" % "2.2.0"