import models.JsonResponse;
import play.http.HttpErrorHandler;
import play.mvc.*;
import play.mvc.Http.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import javax.inject.Singleton;

@Singleton
public class CustomErrorHandler implements HttpErrorHandler {
    public CompletionStage<Result> onClientError(RequestHeader request, int statusCode, String message) {
        if (request.contentType().isPresent() && request.contentType().get().equalsIgnoreCase("application/json")) {
            return CompletableFuture.completedFuture(Results.internalServerError(JsonResponse.error(null, message)));
        } else {
            return CompletableFuture.completedFuture(
                    Results.internalServerError("A server error occurred: " + message)
            );
        }
    }

    public CompletionStage<Result> onServerError(RequestHeader request, Throwable exception) {
        exception.printStackTrace();

        if (request.contentType().isPresent() && request.contentType().get().equalsIgnoreCase("application/json")) {
            return CompletableFuture.completedFuture(Results.internalServerError(JsonResponse.error(null, exception.getMessage())));
        } else {
            return CompletableFuture.completedFuture(
                    Results.internalServerError("A server error occurred: " + exception.getMessage())
            );
        }
    }
}