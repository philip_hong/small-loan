package common;

import models.Application;
import models.User;
import play.data.Form;
import play.mvc.Http;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by philip on 3/16/17.
 */
public class Utils {
    static public final String[] emptyStringArray = new String[] {""};

    static public final ArrayList<Application.Status> allStatus = new ArrayList<>(Arrays.asList(
            Application.Status.LOAN_STATE_START,
            Application.Status.LOAN_STATE_SUBMIT,
            Application.Status.LOAN_STATE_REVIEWED,
            Application.Status.LOAN_STATE_ACCEPTED,
            Application.Status.LOAN_STATE_REJECTED,
            Application.Status.LOAN_STATE_PENDING,
            Application.Status.LOAN_STATE_APPROVED,
            Application.Status.LOAN_STATE_DENIED));

    static public final ArrayList<Application.Status> supervisorStatus = new ArrayList<>(Arrays.asList(
            Application.Status.LOAN_STATE_SUBMIT,
            Application.Status.LOAN_STATE_REVIEWED,
            Application.Status.LOAN_STATE_ACCEPTED,
            Application.Status.LOAN_STATE_REJECTED,
            Application.Status.LOAN_STATE_PENDING,
            Application.Status.LOAN_STATE_APPROVED,
            Application.Status.LOAN_STATE_DENIED));

    static public final ArrayList<Application.Status> managerStatus = new ArrayList<>(Arrays.asList(
            Application.Status.LOAN_STATE_ACCEPTED,
            Application.Status.LOAN_STATE_PENDING,
            Application.Status.LOAN_STATE_APPROVED,
            Application.Status.LOAN_STATE_DENIED));

    static public final ArrayList<Application.Status> statusFilterForUser(User user) {
        if (user.role.equals(User.Function.MANAGER)) {
            return managerStatus;
        } else if (user.role.equals(User.Function.SUPERVISOR)) {
            return supervisorStatus;
        } else if (user.role.equals(User.Function.USER)) {
            return allStatus;
        }

        return new ArrayList<>(Arrays.asList(Application.Status.LOAN_STATE_NONE));
    }

    static public Application.Status statusForAction(Application application, User user, boolean positive) {
        if (user.role.equals(User.Function.MANAGER)) {
            if (    application.status.equals(Application.Status.LOAN_STATE_ACCEPTED) ||
                    application.status.equals(Application.Status.LOAN_STATE_PENDING)) {
                return (positive) ? Application.Status.LOAN_STATE_APPROVED : Application.Status.LOAN_STATE_DENIED;
            }
        } else if (user.role.equals(User.Function.SUPERVISOR)) {
            if (    application.status.equals(Application.Status.LOAN_STATE_REVIEWED) ||
                    application.status.equals(Application.Status.LOAN_STATE_SUBMIT)) {
                return (positive) ? Application.Status.LOAN_STATE_ACCEPTED : Application.Status.LOAN_STATE_REJECTED;
            }
        }

        return null;
    }

    static public String firstFormError(Form<?> form) {
        if (form.hasErrors()) {
            String key = (String) form.errors().keySet().toArray()[0];
            String message = form.error(key).message();
            return message;
        }

        return "位置表格映射错误";
    }

    static public boolean isRole(User.Function role) {
        Map<String, Object> args = Http.Context.current().args;

        if (args.containsKey("user")) {
            User user = (User)args.get("user");
            return user.role.equals(role);
        }

        return false;
    }

    static public boolean verifyAccess(Application application, User user) {
        if (application == null || user == null) return false;

        if (user.role.equals(User.Function.USER)) {
            return (application.agent.id.equals(user.id));
        } else if (user.role.equals(User.Function.SUPERVISOR)) {
            return (application.agent.branch.id.equals(user.branch.id));
        } else if (user.role.equals((User.Function.MANAGER))) {
            return true;
        }

        return false;
    }

    static public int pageSize() {
        return (Http.Context.current().args.containsKey("api")) ? 10 : 4;
    }
}
