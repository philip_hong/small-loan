package actions;

import models.User;
import play.Logger;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by philip on 3/15/17.
 */
public class AdminAction extends Action.Simple {
    @Override
    public CompletionStage<Result> call(Http.Context ctx) {
        if (ctx.args.containsKey("user")) {
            User user = (User)ctx.args.get("user");

            if (user.role.equals(User.Function.ADMIN)) {
                return delegate.call(ctx);
            }
        }

        return CompletableFuture.completedFuture(redirect("/unauthorized"));
    }
}
