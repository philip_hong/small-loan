package actions;

import models.JsonResponse;
import models.Session;
import play.Logger;
import play.db.jpa.Transactional;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import services.SessionService;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by philip on 3/10/17.
 */
public class AuthenticateAction extends Action.Simple {
    final static String authHeaderKey = "X-AUTH-TOKEN";

    @Inject
    SessionService sessionService;

    @Override
    public CompletionStage<Result> call(Http.Context ctx) {
        if (ctx.request().contentType().isPresent()) {
            if (ctx.request().hasHeader(authHeaderKey)) {
                String authToken = ctx.request().getHeader(authHeaderKey);
                Optional<Session> session = sessionService.byToken(authToken);

                if (session.isPresent()) {
                    ctx.args.put("user", session.get().user);
                    ctx.args.put("api", true);
                    return delegate.call(ctx);
                }
            }

            return CompletableFuture.completedFuture(unauthorized(JsonResponse.error(null, "用户未登录，无法操作")));
        } else {
            String authToken = ctx.session().get("token");

            if (authToken != null && authToken.length() > 0) {
                Optional<Session> session = sessionService.byToken(authToken);

                if (session.isPresent()) {
                    ctx.args.put("user", session.get().user);
                    return delegate.call(ctx);
                }
            }

            ctx.session().put("from", ctx.request().uri());
            return CompletableFuture.completedFuture(redirect("/login"));
        }
    }
}
