package controllers.api;

import actions.AuthenticateAction;
import com.fasterxml.jackson.databind.JsonNode;
import common.Utils;
import models.*;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import services.ApplicantService;
import services.ApplicationService;
import services.ValuationService;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.*;

/**
 * Created by philip on 3/13/17.
 */
@Transactional
@With(AuthenticateAction.class)
public class ApplicationController extends Controller {
    @Inject
    private FormFactory formFactory;

    @Inject
    private ValuationService valuationService;

    @Inject
    private ApplicationService applicationService;

    @Inject
    private ApplicantService applicantService;

    @BodyParser.Of(BodyParser.Json.class)
    @Transactional
    public Result create() {
        JsonNode json = request().body().asJson();
        Form<ApplicationCreateRequest> appForm = formFactory.form(ApplicationCreateRequest.class).bind(json);

        if (appForm.hasErrors()) {
            return badRequest(JsonResponse.formError(appForm));
        }

        User agent = (User)ctx().args.get("user");
        ApplicationCreateRequest request = appForm.get();

        // Valuation check
        Optional<Valuation> valuation = valuationService.byId(request.valuationId);
        if (!valuation.isPresent()) {
            return badRequest(JsonResponse.error(null, "资产评估信息不存在"));
        }

        // Applicant check
        Applicant applicant = new Applicant();
        applicant.identificationNumber = request.identificationNumber;
        applicant.name = request.name;
        applicant.phone = request.phone;
        applicant.source = agent.branch;

        Validator v = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Applicant>> applicantErrors = v.validate(applicant);

        if (applicantErrors.size() > 0) {
            return badRequest(JsonResponse.validatorError(applicantErrors.iterator().next()));
        }

        Optional<Applicant> applicantByIdOrPhone = applicantService.byIdentificationNumberOrPhone(request.identificationNumber, request.phone);

        if (applicantByIdOrPhone.isPresent()) {
            Applicant existingApplicant = applicantByIdOrPhone.get();
            if (applicant.phone.equalsIgnoreCase(existingApplicant.phone) && applicant.identificationNumber.equalsIgnoreCase(existingApplicant.identificationNumber)) {
                applicant = existingApplicant;
            } else {
                return badRequest(JsonResponse.error(null, "申请者已经在系统里存在，但与注册的手机或身份证不符"));
            }
        }

        // Media check
        List<Media> extraMedia = new ArrayList<>();

        // Extra check
        if (request.otherMedia != null && request.otherMedia.size() > 0) {
            request.otherMedia.forEach(s -> extraMedia.add(new Media(s, Media.Type.PHOTO)));
        }

        ApplicationMedia mediaBundle = new ApplicationMedia();
        mediaBundle.identificationFront = new Media(request.identificationFront, Media.Type.PHOTO);
        mediaBundle.identificationBack = new Media(request.identificationBack, Media.Type.PHOTO);
        mediaBundle.hukouBack = new Media(request.hukouBack, Media.Type.PHOTO);
        mediaBundle.huKouFront = new Media(request.hukouFront, Media.Type.PHOTO);
        mediaBundle.deed = new Media(request.deed, Media.Type.PHOTO);
        mediaBundle.extraMedia = extraMedia;

        Set<ConstraintViolation<ApplicationMedia>> applicationMediaErrors = v.validate(mediaBundle);

        if (applicationMediaErrors.size() > 0) {
            return badRequest(JsonResponse.validatorError(applicationMediaErrors.iterator().next()));
        }

        // Application
        Application application = new Application();
        application.agent = agent;
        application.applicationMedia = mediaBundle;
        application.applicant = applicant;
        application.type = request.type;
        application.valuation = valuation.get();
        application.address = request.address;
        application.status = Application.Status.LOAN_STATE_SUBMIT;

        Set<ConstraintViolation<Application>> applicationErrors = v.validate(application);

        if (applicationErrors.size() > 0) {
            return badRequest(JsonResponse.validatorError(applicationErrors.iterator().next()));
        }

        applicationService.create(application);

        return ok(JsonResponse.success(application));
    }

    @Transactional(readOnly = true)
    public Result list(int page) {
        User user = (User)ctx().args.get("user");

        PageableObject pageable = applicationService.applicationsForUser(user, page);

        return ok(JsonResponse.success(pageable));
    }

    @Transactional(readOnly = true)
    public Result view(Long applicationId) {
        Optional<Application> app = applicationService.byId(applicationId);
        User user = (User)ctx().args.get("user");

        if (!app.isPresent()) {
            return badRequest(JsonResponse.error(null, "申请不存在"));
        }

        if (!Utils.verifyAccess(app.get(), user)) {
            return badRequest(JsonResponse.error(null, "该用户无查看此申请的权限"));
        }

        Application applicationObject = app.get();
        applicationObject.applicationMedia.extraMedia.toString();

        return ok(JsonResponse.success(applicationObject));
    }
}
