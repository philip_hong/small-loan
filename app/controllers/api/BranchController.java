package controllers.api;

import models.Branch;
import models.JsonResponse;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import services.BranchService;

import javax.inject.Inject;
import java.util.Collection;

/**
 * Created by philip on 4/5/17.
 */
public class BranchController extends Controller {
    @Inject
    private BranchService branchService;

    @Transactional(readOnly = true)
    public Result list() {
        Collection<Branch> branches = branchService.all();
        return ok(JsonResponse.success(branches));
    }
}
