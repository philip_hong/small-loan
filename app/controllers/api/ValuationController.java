package controllers.api;

import actions.AuthenticateAction;
import models.JsonResponse;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import services.ValuationService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by philip on 3/13/17.
 */
@Transactional
@With(AuthenticateAction.class)
public class ValuationController extends Controller {
    final static private String jsonContentType = "application/json";

    @Inject
    ValuationService valuationService;

    ExecutorService executor = Executors.newFixedThreadPool(20);

    public CompletionStage<Result> getProvinces() {
        return CompletableFuture.supplyAsync(() -> valuationService.getProvinces(), executor).thenApply(s -> ok(s).as(jsonContentType));
    }

    public CompletionStage<Result> getCities(Integer cityId) {
        return CompletableFuture.supplyAsync(() -> valuationService.getCities(cityId), executor).thenApply(s -> ok(s).as(jsonContentType));
    }

    public CompletionStage<Result> getConstructions(Integer cityId, String constructionName) {
        return CompletableFuture.supplyAsync(() -> valuationService.getConstructions(cityId, constructionName), executor).thenApply(s -> ok(s).as(jsonContentType));
    }

    public CompletionStage<Result> getBuildings(Integer cityId, Integer constructionId) {
        return CompletableFuture.supplyAsync(() -> valuationService.getBuildings(cityId, constructionId), executor).thenApply(s -> ok(s).as(jsonContentType));
    }

    public CompletionStage<Result> getHouses(Integer cityId, Integer buildingId) {
        return CompletableFuture.supplyAsync(() -> valuationService.getHouses(cityId, buildingId), executor).thenApply(s -> ok(s).as(jsonContentType));
    }

    @Transactional
    public CompletionStage<Result> getPrice(Integer cityId, Integer constructionId, Integer buildingId, Integer houseId, double size) {
        return CompletableFuture.supplyAsync(() -> valuationService.getPrice(cityId, constructionId, buildingId, houseId, size), executor).thenApply(s -> ok(JsonResponse.success(s)));
    }
}
