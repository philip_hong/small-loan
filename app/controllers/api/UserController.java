package controllers.api;

import actions.AuthenticateAction;
import com.fasterxml.jackson.databind.JsonNode;
import models.JsonResponse;
import models.Session;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import services.SessionService;
import services.UserService;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by philip on 3/8/17.
 */
@Transactional
public class UserController extends Controller {
    @Inject
    private UserService userService;

    @Inject
    private SessionService sessionService;

    @Inject
    private FormFactory formFactory;

    @Transactional(readOnly = true)
    @With(AuthenticateAction.class)
    public CompletionStage<Result> index() {
        return CompletableFuture.supplyAsync(() -> userService.all()).thenApply(users -> ok(Json.toJson(users)));
    }

    @Transactional(readOnly = true)
    public Result login(String username, String password) {
        if (username.isEmpty() || username.length() <= 0) {
            return badRequest(JsonResponse.error(null, "登录失败，用户名为空"));
        }

        if (password.isEmpty() || password.length() <= 0) {
            return badRequest(JsonResponse.error(null, "登录失败，密码为空"));
        }

        Optional<User> existingUser = userService.byUsername(username);
        if (!existingUser.isPresent()) {
            return badRequest(JsonResponse.error(null,"登录失败，用户不存在"));
        }

        User existingUserObject = existingUser.get();
        if (!userService.validatePassword(existingUserObject, password)) {
            return badRequest(JsonResponse.error(null,"登录失败"));
        }

        Session session = sessionService.create(existingUserObject, request().getHeader("User-Agent"), request().remoteAddress());
        return ok(JsonResponse.success(session));
    }

    @Transactional()
    @BodyParser.Of(BodyParser.Json.class)
    public Result create() {
        JsonNode json = request().body().asJson();
        Form<User> userForm = formFactory.form(User.class).bind(json);

        if (userForm.hasErrors()) {
            return badRequest(JsonResponse.formError(userForm));
        } else {
            User formUser= userForm.get();

            if (userService.byPhone(formUser.phone).isPresent()) {
                return badRequest(JsonResponse.error(null, "用户电话号码已经被注册"));
            }

            if (userService.byIdentificationNumber(formUser.identificationNumber).isPresent()) {
                return badRequest(JsonResponse.error(null, "用户身份证已经被注册"));
            }

            return ok(JsonResponse.success(userService.create(formUser)));
        }
    }

    @Transactional
    @With(AuthenticateAction.class)
    @BodyParser.Of(BodyParser.Json.class)
    public Result password() {
        JsonNode json = request().body().asJson();

        if (!json.hasNonNull("password")) {
            return badRequest(JsonResponse.error(null, "缺少老密码"));
        }

        String password = json.get("password").textValue();

        User user = (User)ctx().args.get("user");
        if (!userService.validatePassword(user, password)) {
            return badRequest(JsonResponse.error(null, "用户信息不正确"));
        }

        if (!json.hasNonNull("new_password")) {
            return badRequest(JsonResponse.error(null, "缺少新密码"));
        }

        String newPassword = json.get("new_password").textValue();

        if (newPassword.isEmpty()) {
            return badRequest(JsonResponse.error(null, "密码不能为空"));
        }

        userService.updatePassword(user, newPassword);

        return ok(JsonResponse.success(null));
    }
}
