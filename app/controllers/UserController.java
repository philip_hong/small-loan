package controllers;

import actions.AdminAction;
import actions.AuthenticateAction;
import common.Utils;
import models.Branch;
import models.User;
import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.javase.QRCode;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import services.BranchService;
import services.UserService;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by philip on 3/17/17.
 */

@Transactional
@With(AuthenticateAction.class)
public class UserController extends Controller {
    @Inject
    UserService userService;

    @Inject
    BranchService branchService;

    @Inject
    FormFactory formFactory;

    ExecutorService executor = Executors.newFixedThreadPool(20);

    @Transactional(readOnly = true)
    public Result index() {
        Collection<User> users = userService.all();
        return ok(views.html.user.index.render(users));
    }

    @Transactional
    public Result showReferralQRCode() {
        User user = (User)ctx().args.get("user");
        String referralUrl = routes.RegisterController.showAdd(user.referralCode).absoluteURL(ctx().request());
        return ok(QRCode.from(referralUrl).withSize(250, 250).to(ImageType.PNG).stream().toByteArray()).as("image/png");
    }

    @Transactional
    @With(AdminAction.class)
    public CompletionStage<Result> refreshReferralQRCode() {
        return CompletableFuture.supplyAsync(() -> {
            userService.refreshReferralToken();
            return CompletableFuture.completedFuture(null);
        }, executor).thenApply(u -> redirect(routes.UserController.index()));
    }

    @Transactional
    @With(AdminAction.class)
    public Result toggle(Long userId) {
        Optional<User> user = userService.byId(userId);

        if (!user.isPresent()) {
            return redirect(controllers.routes.HomeController.showNotFound());
        }

        User userObject = user.get();
        userObject.enabled = !userObject.enabled;

        userService.save(userObject);
        return redirect(controllers.routes.UserController.index());
    }

    @Transactional(readOnly = true)
    @With(AdminAction.class)
    public Result showAdd() {
        Form<User> userForm = formFactory.form(User.class);

        Collection<Branch> branches = branchService.all();

        return ok(views.html.user.edit.render(userForm, new Long(-1), branches));
    }

    @Transactional
    @With(AdminAction.class)
    public Result doAdd() {
        Form<User> form = formFactory.form(User.class).bindFromRequest();

        Collection<Branch> branches = branchService.all();

        if (form.hasErrors()) {
            flash("error", Utils.firstFormError(form));
            return ok(views.html.user.edit.render(form, new Long(-1), branches));
        } else {
            User formUser= form.get();

            if (userService.byPhone(formUser.phone).isPresent()) {
                flash("error", "用户电话号码已经被注册");
                return ok(views.html.user.edit.render(form, new Long(-1), branches));
            }

            if (userService.byIdentificationNumber(formUser.identificationNumber).isPresent()) {
                flash("error", "用户身份证已经被注册");
                return ok(views.html.user.edit.render(form, new Long(-1), branches));
            }

            Long newBranchId = form.get().branch.id;
            Optional<Branch> newBranch = branchService.byId(newBranchId);

            if (!newBranch.isPresent()) {
                flash("error", "渠道号不存在");
                return ok(views.html.user.edit.render(form, new Long(-1), branches));
            }

            formUser.branch = newBranch.get();

            userService.create(formUser);
        }

        return redirect(controllers.routes.UserController.index());
    }

    @Transactional(readOnly = true)
    @With(AdminAction.class)
    public Result showEdit(Long userId) {
        Optional<User> user = userService.byId(userId);

        if (!user.isPresent()) {
            return redirect(controllers.routes.HomeController.showNotFound());
        }

        Form<User> userForm = formFactory.form(User.class).fill(user.get());

        Collection<Branch> branches = branchService.all();

        return ok(views.html.user.edit.render(userForm, userId, branches));
    }

    @Transactional
    @With(AdminAction.class)
    public Result doEdit(Long userId) {
        Form<User> form = formFactory.form(User.class).bindFromRequest();
        form.discardErrors();

        Optional<User> user = userService.byId(userId);
        if (!user.isPresent()) {
            return redirect(controllers.routes.HomeController.showNotFound());
        }

        Collection<Branch> branches = branchService.all();

        User userObject = userService.detach(user.get());

        Optional<User> userByPhone = userService.byPhone(form.get().phone);
        if (userByPhone.isPresent() && !userByPhone.get().id.equals(userObject.id)) {
            flash("error", "手机号已经被注册");
            return ok(views.html.user.edit.render(form, userId, branches));
        }

        Optional<User> userByIdentificationNumber = userService.byIdentificationNumber(form.get().identificationNumber);
        if (userByIdentificationNumber.isPresent() && !userByIdentificationNumber.get().id.equals(userObject.id)) {
            flash("error", "身份证已经被注册");
            return ok(views.html.user.edit.render(form, userId, branches));
        }

        Long newBranchId = form.get().branch.id;
        Optional<Branch> newBranch = branchService.byId(newBranchId);

        if (!newBranch.isPresent()) {
            flash("error", "渠道号不存在");
            return ok(views.html.user.edit.render(form, userId, branches));
        }

        userObject.phone = form.get().phone;
        userObject.identificationNumber = form.get().identificationNumber;
        userObject.name = form.get().name;
        userObject.role = form.get().role;
        userObject.branch = newBranch.get();

        Validator v = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<User>> errors = v.validate(userObject);

        if (errors.size() > 0) {
            flash("error", errors.iterator().next().getMessage());
            return ok(views.html.user.edit.render(form, userId, branches));
        }

        userService.save(userObject);

        return redirect(controllers.routes.UserController.index());
    }
}