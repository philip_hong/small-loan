package controllers;

import actions.AuthenticateAction;
import common.Utils;
import controllers.*;
import controllers.routes;
import models.Application;
import models.PageableObject;
import models.User;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import services.ApplicationService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by philip on 3/20/17.
 */
@Transactional
@With(AuthenticateAction.class)
public class ApplicationController extends Controller {
    @Inject
    ApplicationService applicationService;

    @Transactional(readOnly = true)
    public Result index(int page) {
        User user = (User)ctx().args.get("user");

        PageableObject pageable = applicationService.applicationsForUser(user, page);

        return ok(views.html.application.index.render(pageable));
    }

    @Transactional(readOnly = true)
    public Result view(Long applicationId) {
        Optional<Application> app = applicationService.byId(applicationId);
        User user = (User)ctx().args.get("user");

        if (!app.isPresent()) {
            return redirect(controllers.routes.HomeController.showNotFound());
        }

        if (!Utils.verifyAccess(app.get(), user)) {
            return redirect(controllers.routes.HomeController.showAccessDenied());
        }

        Application.Status status1 = Utils.statusForAction(app.get(), user, true);
        Application.Status status2 = Utils.statusForAction(app.get(), user, true);

        return ok(views.html.application.view.render(app.get(), user, status1, status2));
    }

    @Transactional
    public Result action(Long applicationId, boolean approve) {
        Optional<Application> app = applicationService.byId(applicationId);
        User user = (User)ctx().args.get("user");

        if (!app.isPresent()) {
            return redirect(controllers.routes.HomeController.showNotFound());
        }

        if (!user.role.equals(User.Function.SUPERVISOR) && !user.role.equals(User.Function.MANAGER)) {
            return redirect(controllers.routes.HomeController.showAccessDenied());
        }

        Application appObject = app.get();

        if (!Utils.verifyAccess(appObject, user)) {
            return redirect(controllers.routes.HomeController.showAccessDenied());
        }

        Application.Status status = Utils.statusForAction(appObject, user, approve);

        if (status != null) {
            appObject.status = status;
            applicationService.save(appObject);
            return redirect(controllers.routes.ApplicationController.view(appObject.id));
        } else {
            return redirect(controllers.routes.HomeController.showAccessDenied());
        }
    }
}
