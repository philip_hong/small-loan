package controllers;

import actions.AuthenticateAction;
import models.Session;
import models.User;
import play.db.jpa.Transactional;
import play.mvc.*;
import services.SessionService;
import services.UserService;

import javax.inject.Inject;
import java.util.Map;
import java.util.Optional;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */

public class HomeController extends Controller {
    @Inject
    SessionService sessionService;

    @Inject
    UserService userService;

    @Transactional()
    @With(AuthenticateAction.class)
    public Result index() {
        User user = (User)ctx().args.get("user");

        return ok(views.html.index.render(user));
    }

    public Result showLogin() {
        return ok(views.html.login.render());
    }

    @Transactional()
    public Result doLogin() {
        Map<String, String[]> params = request().body().asFormUrlEncoded();

        if (!params.containsKey("phone") || !params.containsKey("password")) {
            flash("error", "登录失败，电话或者密码不能为空");
        } else {
            String phone = params.get("phone")[0];
            String password = params.get("password")[0];

            Optional<User> user = userService.byUsername(phone);

            if (user.isPresent()) {
                if (!userService.validatePassword(user.get(), password)) {
                    flash("error", "登录失败，密码错误");
                } else {
                    Session session = sessionService.create(user.get(), request().getHeader("User-Agent"), request().remoteAddress());
                    session("token", session.token);

                    String fromUri = session("from");
                    session().remove("from");

                    if (fromUri != null && fromUri.length() > 0) {
                        return redirect(fromUri);
                    } else {
                        return redirect(routes.HomeController.index());
                    }
                }
            } else {
                flash("error", "用户不存在");
            }
        }

        return ok(views.html.login.render());
    }

    public Result doLogout() {
        session().clear();
        return redirect(routes.HomeController.index());
    }

    @Transactional(readOnly = true)
    @With(AuthenticateAction.class)
    public Result showAccessDenied() {
        return unauthorized(views.html.unauthorized.render());
    }

    @Transactional(readOnly = true)
    @With(AuthenticateAction.class)
    public Result showNotFound() {
        return notFound(views.html.notfound.render());
    }
}
