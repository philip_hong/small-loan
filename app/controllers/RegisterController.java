package controllers;

import common.Utils;
import models.User;
import models.UserCreateRequest;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import services.BranchService;
import services.UserService;

import javax.inject.Inject;
import java.util.Optional;

/**
 * Created by philip on 4/1/17.
 */

public class RegisterController extends Controller {
    @Inject
    UserService userService;

    @Inject
    BranchService branchService;

    @Inject
    FormFactory formFactory;

    @Transactional(readOnly = true)
    public Result showAdd(String code) {
        Optional<User> user = userService.byReferralCode(code);

        if (!user.isPresent()) {
            return redirect(routes.HomeController.doLogin());
        }

        Form<UserCreateRequest> userForm = formFactory.form(UserCreateRequest.class);

        return ok(views.html.register.render(userForm, user.get()));
    }

    @Transactional
    public Result doAdd(String code) {
        Optional<User> user = userService.byReferralCode(code);

        if (!user.isPresent()) {
            return redirect(routes.HomeController.doLogin());
        }

        Form<UserCreateRequest> form = formFactory.form(UserCreateRequest.class).bindFromRequest();
        User userObject = user.get();

        if (form.hasErrors()) {
            flash("error", Utils.firstFormError(form));
            return ok(views.html.register.render(form, userObject));
        } else {
            UserCreateRequest formUser= form.get();

            if (userService.byPhone(formUser.phone).isPresent()) {
                flash("error", "用户电话号码已经被注册");
                return ok(views.html.register.render(form, userObject));
            }

            if (userService.byIdentificationNumber(formUser.identificationNumber).isPresent()) {
                flash("error", "用户身份证已经被注册");
                return ok(views.html.register.render(form, userObject));
            }


            User newUser = new User();

            newUser.branch = userObject.branch;
            newUser.password = formUser.password;
            newUser.name = formUser.password;
            newUser.role = User.Function.USER;
            newUser.phone = formUser.phone;
            newUser.identificationNumber = formUser.identificationNumber;

            userService.create(newUser);
        }

        return redirect(controllers.routes.RegisterController.showDownload());
    }

    public Result showDownload() {
        return ok(views.html.registered.render());
    }
}
