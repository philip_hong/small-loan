package controllers;

import actions.AdminAction;
import actions.AuthenticateAction;
import common.Utils;
import models.Branch;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import services.BranchService;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by philip on 3/16/17.
 */

@Transactional
@With(AuthenticateAction.class)
public class BranchController extends Controller {
    @Inject
    BranchService branchService;

    @Inject
    FormFactory formFactory;

    @Transactional(readOnly = true)
    public Result index() {
        Collection<Branch> branches = branchService.all();

        return ok(views.html.branch.index.render(branches));
    }

    @Transactional
    @With(AdminAction.class)
    public Result toggle(Long branchId) {
        Optional<Branch> branch = branchService.byId(branchId);

        if (!branch.isPresent()) {
            return redirect(controllers.routes.HomeController.showNotFound());
        }

        Branch branchObject = branch.get();
        branchObject.enabled = !branchObject.enabled;
        branchService.save(branch.get());

        return redirect(controllers.routes.BranchController.index());
    }

    @With(AdminAction.class)
    public Result showAdd() {
        Form<Branch> form = formFactory.form(Branch.class);
        return ok(views.html.branch.edit.render(form, new Long(-1)));
    }

    @Transactional
    @With(AdminAction.class)
    public Result doAdd() {
        Form<Branch> form = formFactory.form(Branch.class).bindFromRequest();

        if (form.hasErrors()) {
            flash("error", Utils.firstFormError(form));
            return ok(views.html.branch.edit.render(form, new Long(-1)));
        }

        Optional<Branch> branch = branchService.byNameOrCode(form.data().get("name"), form.data().get("code"));
        if (branch.isPresent()) {
            flash("error", "渠道已经存在");
            return ok(views.html.branch.edit.render(form, new Long(-1)));
        }

        branchService.create(form.get());

        return redirect(controllers.routes.BranchController.index());
    }

    @Transactional(readOnly = true)
    @With(AdminAction.class)
    public Result showEdit(Long branchId) {
        Optional<Branch> branch = branchService.byId(branchId);

        if (!branch.isPresent()) {
            return redirect(controllers.routes.HomeController.showNotFound());
        }

        Form<Branch> branchForm = formFactory.form(Branch.class).fill(branch.get());

        return ok(views.html.branch.edit.render(branchForm, branchId));
    }

    @Transactional
    @With(AdminAction.class)
    public Result doEdit(Long branchId) {
        Form<Branch> form = formFactory.form(Branch.class).bindFromRequest();

        if (form.hasErrors()) {
            flash("error", Utils.firstFormError(form));
            return ok(views.html.branch.edit.render(form, branchId));
        }

        Optional<Branch> branch = branchService.byId(branchId);

        if (!branch.isPresent()) {
            return redirect(controllers.routes.HomeController.showNotFound());
        }

        Branch branchObject = branchService.detach(branch.get());

        Optional<Branch> existingBranchByName = branchService.byName(form.get().name);
        if (existingBranchByName.isPresent() && !existingBranchByName.get().id.equals(branchObject.id)) {
            flash("error", "渠道名称已经存在");
            return ok(views.html.branch.edit.render(form, branchId));
        }

        Optional<Branch> existingBranchByCode = branchService.byCode(form.get().code);

        if (existingBranchByCode.isPresent() && !existingBranchByCode.get().id.equals(branchObject.id)) {
            flash("error", "渠道代码已经存在");
            return ok(views.html.branch.edit.render(form, branchId));
        }

        branchObject.name = form.get().name;
        branchObject.code = form.get().code;

        branchService.save(branchObject);

        return redirect(controllers.routes.BranchController.index());
    }
}
