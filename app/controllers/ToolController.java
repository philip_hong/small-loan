package controllers;

import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by philip on 4/10/17.
 */
public class ToolController extends Controller {
    public Result doclist() {
        return ok(views.html.tools.doclist.render());
    }

    public Result steps() {
        return ok(views.html.tools.steps.render());
    }

    public Result calculator() {
        return ok(views.html.tools.calculator.render());
    }
}
