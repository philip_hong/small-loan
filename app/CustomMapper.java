import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import play.Logger;
import play.libs.Json;

/**
 * Created by philip on 3/8/17.
 */
public class CustomMapper {
    public CustomMapper() {
        ObjectMapper mapper = Json.newDefaultMapper();
        mapper.registerModule(new Hibernate5Module());
        Json.setObjectMapper(mapper);

        Logger.info("Loading custom mapper");
    }
}
