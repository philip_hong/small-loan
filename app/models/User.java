package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

/**
 * Created by philip on 3/7/17.
 */
@Entity
public class User {
    public enum Function {
        GUEST("GUEST", "访客"),
        USER("USER", "业务员"),
        SUPERVISOR("SUPERVISOR", "渠道经理"),
        MANAGER("MANAGER", "客户经理"),
        ADMIN("ADMIN", "管理员");

        public String name;
        public String value;

        Function(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }

    final static public EnumSet<Function> functions = EnumSet.allOf(Function.class);

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;

    @NotNull(message = "名字不能为空")
    public String name;

    @NotNull(message = "电话号码不能为空")
    @Column(unique = true)
    @Length(min = 11, max = 11, message = "电话号码格式不正确")
    public String phone;

    @NotNull(message = "身份证不能为空")
    @Length(min = 18, max = 18, message = "身份证格式不正确")
    @Column(name = "identification_number", unique = true)
    public String identificationNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    public User supervisor;

    @OneToMany(mappedBy = "supervisor", fetch = FetchType.LAZY)
    public Collection<User> agents;

    @Enumerated(EnumType.STRING)
    public Function role = Function.USER;

    @NotNull(message = "密码不能为空")
    @Length(min = 6, message = "密码格式不正确")
    @JsonIgnore
    public String password;

    @JsonIgnore
    public String referralCode;

    @OneToOne(fetch = FetchType.EAGER)
    public Branch branch;

    public Boolean enabled = true;
}
