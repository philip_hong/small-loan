package models;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * Created by philip on 3/13/17.
 */
@Entity
@Table(name = "Application_Media")
public class ApplicationMedia {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;

    @OneToOne
    @JoinColumn(name = "identification_front_id")
    @NotNull(message = "身份证正面照片不能为空")
    @Cascade(CascadeType.PERSIST)
    public Media identificationFront;

    @OneToOne
    @JoinColumn(name = "identification_back_id")
    @NotNull(message = "身份证反面照片不能为空")
    @Cascade(CascadeType.PERSIST)
    public Media identificationBack;

    @OneToOne
    @JoinColumn(name = "hukou_front_id")
    @NotNull(message = "户口正面照片不能为空")
    @Cascade(CascadeType.PERSIST)
    public Media huKouFront;

    @OneToOne
    @JoinColumn(name = "hukou_back_id")
    @NotNull(message = "户口反面照片不能为空")
    @Cascade(CascadeType.PERSIST)
    public Media hukouBack;

    @OneToOne
    @NotNull(message = "房产证照片不能为空")
    @Cascade(CascadeType.PERSIST)
    public Media deed;

    @OneToMany(fetch = FetchType.LAZY)
    @Cascade(CascadeType.PERSIST)
    public Collection<Media> extraMedia;
}
