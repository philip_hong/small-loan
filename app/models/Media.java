package models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by philip on 3/13/17.
 */
@Entity
public class Media {
    public enum Type {
        PHOTO,
        VIDEO,
        OTHER
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;

    @Enumerated(EnumType.STRING)
    public Type type = Type.PHOTO;

    @NotNull(message = "资源不能为空")
    public String source;

    public Media(String source, Type type) {
        this.source = source;
        this.type = type;
    }

    public Media() {}
}
