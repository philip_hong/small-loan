package models;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by philip on 3/13/17.
 */

@Entity
public class Applicant {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;

    @NotNull(message = "申请人名字不能为空")
    public String name;

    @NotNull(message = "申请人电话号码不能为空")
    @Column(unique = true)
    @Length(min = 11, max = 11, message = "电话号码格式不正确")
    public String phone;

    @NotNull(message = "申请人身份证不能为空")
    @Length(min = 18, max = 18, message = "身份证格式不正确")
    @Column(name = "identification_number", unique = true)
    public String identificationNumber;

    @NotNull(message = "申请人引进渠道不能为空")
    @OneToOne(fetch = FetchType.LAZY)
    public Branch source;
}
