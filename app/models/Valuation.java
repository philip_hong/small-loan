package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by philip on 3/13/17.
 */
@Entity
public class Valuation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;

    @JsonIgnore
    @Column(name = "city_id")
    public Integer cityId;

    @JsonIgnore
    @Column(name = "construction_id")
    public Integer constructionId;

    @JsonIgnore
    @Column(name = "building_id")
    public Integer buildingId;

    @JsonIgnore
    @Column(name = "house_id")
    public Integer houseId;

    @JsonProperty("BuildArea")
    public Double size;

    @JsonProperty("UnitPrice")
    @Column(name = "unit_price")
    public double unitPrice;

    @JsonProperty("AvagePrice")
    @Column(name = "average_price")
    public double averagePrice;

    @JsonProperty("Amount")
    public double amount;

    @JsonProperty("MaxPrice")
    @Column(name = "max_price")
    public double maxPrice;

    @JsonProperty("MinPrice")
    @Column(name = "min_price")
    public double minPrice;

    @JsonProperty("CaseCount")
    @Column(name = "case_count")
    public Integer caseCount;

    @JsonProperty("Status")
    public Integer status;

    @JsonProperty("Note")
    public String note;

    @JsonIgnore
    public Date created;

    @PrePersist
    void createAt() {
        this.created = new Date();
    }
}
