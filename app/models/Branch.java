package models;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Created by philip on 3/10/17.
 */
@Entity
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;

    @NotNull(message = "渠道名称不能为空")
    @Length(min = 1, message = "渠道名称不能为空")
    public String name;

    @NotNull(message = "渠道代码不能为空")
    @Length(min = 4, max = 4, message = "渠道代码必须为四位")
    public String code;

    public boolean enabled = true;
}
