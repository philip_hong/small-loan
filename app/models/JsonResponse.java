package models;

import com.fasterxml.jackson.databind.JsonNode;
import play.data.Form;
import play.libs.Json;
import play.mvc.Http;

import javax.validation.ConstraintViolation;

/**
 * Created by philip on 3/8/17.
 */
public class JsonResponse {
    public Object data;
    public Integer code;
    public String message;

    public JsonResponse(Object data) {
        this.data = data;
    }

    public static JsonNode formError(Form form) {
        String key = (String)form.errors().keySet().toArray()[0];
        String message = form.error(key).message();
        return error(form.data(), message);
    }

    public static JsonNode validatorError(ConstraintViolation<?> errors) {
        return error(null, errors.getMessage());
    }

    public static JsonNode error(Object data, String message) {
        JsonResponse resp = new JsonResponse(data);
        resp.code = Http.Status.BAD_REQUEST;
        resp.message = message != null ? message: "出错啦，请稍后再试!";
        return Json.toJson(resp);
    }

    public static JsonNode success(Object data) {
        JsonResponse resp = new JsonResponse(data);
        resp.code = Http.Status.OK;
        return Json.toJson(resp);
    }
}
