package models;

import common.Utils;

import java.util.Collection;

/**
 * Created by philip on 3/20/17.
 */
public class PageableObject {
    public Collection<?> items;
    public int page;
    public int total;

    public PageableObject(Collection<?> items, int page, int total) {
        this.items = items;
        this.total = total;
        this.page = page;
    }

    public int maxPage() {
        return (int)Math.ceil(this.total / (Utils.pageSize() * 1.0));
    }

    public boolean hasNext() {
        return (total - page * Utils.pageSize() - items.size() > 0);
    }
}
