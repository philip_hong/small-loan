package models;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

/**
 * Created by philip on 3/13/17.
 */
public class ApplicationCreateRequest {
    // Applicant
    @NotNull(message = "申请人名字不能为空")
    public String name;

    @NotNull(message = "电话号码不能为空")
    @Length(min = 11, max = 11, message = "电话号码格式不正确")
    public String phone;

    @NotNull(message = "身份证不能为空")
    @Length(min = 18, max = 18, message = "身份证格式不正确")
    public String identificationNumber;

    // Application
    @NotNull(message = "贷款种类不能为空")
    public Application.Type type;

    @Min(value = 1, message = "贷款期限最小为一个月")
    @Max(value = 12, message = "贷款期限最大为一年")
    public Integer duration;

    @Min(value = 10000, message = "贷款金额不能小于 10,000")
    public Double amount;

    // media
    @NotNull(message = "身份证正面照片不能为空")
    public String identificationFront;

    @NotNull(message = "身份证反面照片不能为空")
    public String identificationBack;

    @NotNull(message = "户口正面照片不能为空")
    public String hukouFront;

    @NotNull(message = "户口反面照片不能为空")
    public String hukouBack;

    @NotNull(message = "房产证照片不能为空")
    public String deed;

    // others
    public List<String> otherMedia;

    // valuation
    @NotNull(message = "资产评估不能为空")
    public Long valuationId;

    @NotNull(message = "地址不能为空")
    public String address;
}
