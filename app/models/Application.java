package models;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by philip on 3/13/17.
 */
@Entity
public class Application {
    public enum Status {
        LOAN_STATE_NONE("LOAN_STATE_START", 0, "无效"),           // 开始起草
        LOAN_STATE_START("LOAN_STATE_START", 10, "起草"),           // 开始起草
        LOAN_STATE_SUBMIT("LOAN_STATE_SUBMIT", 20, "提交"),         // 提交
        LOAN_STATE_REVIEWED("LOAN_STATE_REVIEWED", 30, "待审核"),     // 待审核
        LOAN_STATE_ACCEPTED("LOAN_STATE_ACCEPTED", 40, "审核通过"),     // 审核通过
        LOAN_STATE_REJECTED("LOAN_STATE_REJECTED", 41, "审核不通过"),     // 审核不通过
        LOAN_STATE_PENDING("LOAN_STATE_PENDING", 50, "待批"),       // 待批
        LOAN_STATE_APPROVED("LOAN_STATE_APPROVED", 60, "批准"),     // 批准
        LOAN_STATE_DENIED("LOAN_STATE_DENIED", 61, "不批准");         // 不批准

        public String name;
        public Integer value;
        public String text;

        Status(String name, Integer value, String text) {
            this.name = name;
            this.value = value;
            this.text = text;
        }
    }

    public enum Type {
        LOAN_TYPE_1("LOAN_TYPE_1", "贷款种类一"),
        LOAN_TYPE_2("LOAN_TYPE_2", "贷款种类二"),
        LOAN_TYPE_3("LOAN_TYPE_3", "贷款种类三");

        public String name;
        public String text;

        Type(String name, String text) {
            this.name = name;
            this.text = text;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull(message = "录入人不能为空")
    public User agent;

    @OneToOne(fetch = FetchType.EAGER)
    @NotNull(message = "申请人不能为空")
    @Cascade(CascadeType.PERSIST)
    public Applicant applicant;

    @Enumerated(EnumType.STRING)
    public Status status = Status.LOAN_STATE_START;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "贷款种类不能为空")
    public Type type;

    @Min(value = 1, message = "贷款期限最小为一个月")
    @Max(value = 12, message = "贷款期限最大为一年")
    public Integer duration = 1;

    @Min(value = 10000, message = "贷款金额不能小于 10,000")
    public Double amount;

    @NotNull(message = "估值信息不能为空")
    @OneToOne(fetch = FetchType.EAGER)
    public Valuation valuation;

    @NotNull(message = "申请图片资料不能为空")
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_media_id")
    @Cascade(CascadeType.PERSIST)
    public ApplicationMedia applicationMedia;

    @NotNull(message = "地址不能为空")
    public String address;

    public Date created;

    @PrePersist
    void createAt() {
        this.created = new Date();
    }
}
