package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

/**
 * Created by philip on 4/1/17.
 */
public class UserCreateRequest {
    @NotNull(message = "名字不能为空")
    public String name;

    @NotNull(message = "电话号码不能为空")
    @Column(unique = true)
    @Length(min = 11, max = 11, message = "电话号码格式不正确")
    public String phone;

    @NotNull(message = "身份证不能为空")
    @Length(min = 18, max = 18, message = "身份证格式不正确")
    @Column(name = "identification_number", unique = true)
    public String identificationNumber;

    @NotNull(message = "密码不能为空")
    @Length(min = 6, message = "密码格式不正确")
    @JsonIgnore
    public String password;
}
