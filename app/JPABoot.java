import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by philip on 3/7/17.
 */
@Singleton
public class JPABoot {
    @Inject public JPABoot(JPAApi api) {
        Logger.info("Bootstraping entity manager");
        api.withTransaction(() -> { api.em(); });
    }
}
