package services;

import javax.inject.Singleton;
import javax.persistence.NoResultException;
import java.util.Optional;

/**
 * Created by philip on 3/9/17.
 */
@Singleton
public class DBQuery {
    @FunctionalInterface
    public interface DaoRetriever<T> {
        T retrieve() throws NoResultException;
    }

    public static <T> Optional<T> findOrEmpty(final DaoRetriever<T> retriever) {
        try {
            return Optional.of(retriever.retrieve());
        } catch (NoResultException ex) {
            //log
        }
        return Optional.empty();
    }
}
