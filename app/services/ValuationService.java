package services;

import models.Valuation;
import org.apache.commons.codec.digest.DigestUtils;
import org.tempuri.IQueryPriceServer;
import org.tempuri.QueryPriceServer;
import play.Logger;
import play.db.jpa.JPAApi;
import play.libs.Json;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

/**
 * Created by philip on 3/13/17.
 */
@Singleton
public class ValuationService {
    private final static String baseURL     = "http://io.worldunion.cn:8307/QueryPriceServer.svc?wsdl";
    private final static String username    = "SLtest";
    private final static String password    = "test123.com";
    private final static String token       = DigestUtils.md5Hex(username + password);
    private final static String tempURL     = "http://tempuri.org/";
    private final static String endPoint    = "QueryPriceServer";

    @Inject
    private JPAApi db;

    private IQueryPriceServer getService() {
        QName serviceName = new QName(tempURL, endPoint);
        QueryPriceServer service;

        try {
            service = new QueryPriceServer(new URL(baseURL), serviceName);
        } catch (MalformedURLException ex) {
            Logger.error("Unable to create base url object for valuation service");
            return null;
        }

        IQueryPriceServer port = service.getBasicHttpBindingIQueryPriceServer();
        ((BindingProvider) port).getRequestContext().put("com.sun.xml.ws.connect.timeout", 5000);

        return port;
    }

    public String getProvinces() {
        String resp = getService().getProvince(username, token);
        Logger.debug(resp);
        return resp;
    }

    public String getCities(Integer cityId) {
        String resp = getService().getCity(cityId, username, token);
        Logger.debug(resp);
        return resp;
    }

    public String getConstructions(Integer cityId, String constructionName) {
        String resp = getService().getConstruction(constructionName, cityId, username, token);
        Logger.debug(resp);
        return resp;
    }

    public String getBuildings(Integer cityId, Integer constructionId) {
        String resp = getService().getBuilding(constructionId, cityId, username, token);
        Logger.debug(resp);
        return resp;
    }

    public String getHouses(Integer cityId, Integer buildingId) {
        String resp = getService().getHouse(buildingId, cityId, username, token);
        Logger.debug(resp);
        return resp;
    }

    public Valuation getPrice(Integer cityId, Integer constructionId, Integer buildingId, Integer houseId, Double size) {
        Optional<Valuation> valuation = DBQuery.findOrEmpty(() ->
            db.withTransaction(() ->
                db.em().createQuery("select v from Valuation v where " +
                        "v.cityId = ?1 and " +
                        "v.constructionId = ?2 and " +
                        "v.buildingId = ?3 and " +
                        "v.houseId = ?4 and " +
                        "v.size = ?5", Valuation.class)
                        .setParameter(1, cityId)
                        .setParameter(2, constructionId)
                        .setParameter(3, buildingId)
                        .setParameter(4, houseId)
                        .setParameter(5, size)
                        .setMaxResults(1).getSingleResult()
            )
        );

        if (valuation.isPresent()) {
            return valuation.get();
        } else {
            String resp = getService().getAutoPrice2(cityId, constructionId, buildingId, houseId, size, username, token);

            Valuation newValuation = Json.fromJson(Json.parse(resp), Valuation.class);
            newValuation.cityId = cityId;
            newValuation.constructionId = constructionId;
            newValuation.buildingId = buildingId;
            newValuation.houseId = houseId;
            newValuation.size = size;

            db.withTransaction(() ->
                db.em().persist(newValuation)
            );

            return newValuation;
        }
    }

    public Optional<Valuation> byId(Long id) {
        return DBQuery.findOrEmpty(() ->
            db.em().createQuery("select v from Valuation v where v.id = ?1", Valuation.class)
                .setParameter(1, id)
                .setMaxResults(1)
                .getSingleResult()
        );
    }
}
