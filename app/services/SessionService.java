package services;

import models.Session;
import models.User;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by philip on 3/10/17.
 */

@Singleton
public class SessionService {
    @Inject
    private JPAApi db;

    public Session create(User user, String userAgent, String ip) {
        Session newSession = new Session();

        newSession.enabled = true;
        newSession.user = user;
        newSession.userAgent = userAgent;
        newSession.ipAddress = ip;
        newSession.token = UUID.randomUUID().toString();

        db.withTransaction(() -> db.em().persist(newSession));
        return newSession;
    }

    public Optional<Session> byToken(String token) {
        return DBQuery.findOrEmpty(() ->
                db.em().createQuery("select s from Session s where s.token = ?1 and s.enabled = true order by id desc ", Session.class)
                        .setParameter(1, token)
                        .setMaxResults(1)
                        .getSingleResult()
        );
    }
}
