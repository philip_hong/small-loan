package services;

import models.Branch;
import models.User;
import org.mindrot.jbcrypt.BCrypt;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by philip on 3/8/17.
 */

@Singleton
public class UserService {
    @Inject
    private JPAApi db;

    public User detach(User user) {
        db.em().detach(user);
        return user;
    }

    public User create(User user) {
        user.password = BCrypt.hashpw(user.password, BCrypt.gensalt());
        user.referralCode = UUID.randomUUID().toString().replace("-", "");
        db.withTransaction(() -> db.em().persist(user));
        return user;
    }

    public User updatePassword(User user, String password) {
        user.password = BCrypt.hashpw(password, BCrypt.gensalt());
        db.withTransaction(() -> db.em().merge(user));
        return user;
    }

    public void save(User user) {
        db.em().merge(user);
    }

    public Boolean validatePassword(User user, String password) {
        return BCrypt.checkpw(password, user.password);
    }


    public Optional<User> byId(Long id) {
        return DBQuery.findOrEmpty(() ->
            db.em().createQuery("select u from User u where u.id = ?1", User.class)
                .setMaxResults(1)
                .setParameter(1, id)
                .getSingleResult()
        );
    }

    public Optional<User> byUsername(String username) {
        return DBQuery.findOrEmpty(() ->
                db.em().createQuery("select u from User u where u.phone = ?1", User.class)
                        .setParameter(1, username)
                        .setMaxResults(1)
                        .getSingleResult()
        );
    }

    public Optional<User> byPhone(String phone) {
        return DBQuery.findOrEmpty(() ->
                db.em().createQuery("select u from User u where u.phone = ?1", User.class)
                        .setParameter(1, phone)
                        .setMaxResults(1)
                        .getSingleResult()
        );
    }

    public Optional<User> byIdentificationNumber(String number) {
        return DBQuery.findOrEmpty(() ->
                db.em().createQuery("select u from User u where u.identificationNumber = ?1", User.class)
                        .setParameter(1, number)
                        .setMaxResults(1)
                        .getSingleResult()
        );
    }

    public Optional<User> byReferralCode(String code) {
        return DBQuery.findOrEmpty(() ->
            db.em().createQuery("select u from User u where u.referralCode = ?1", User.class)
                .setParameter(1, code)
                .setMaxResults(1)
                .getSingleResult()
        );
    }

    public Collection<Long> byBranch(Branch branch) {
        return db.em().createQuery("select u.id from User u where u.branch.id = ?1", Long.class)
                .setParameter(1, branch.id)
                .getResultList();
    }

    public Collection<User> all() {
        return db.em().createQuery("select u from User u").getResultList();
    }

    public void refreshReferralToken() {
        db.withTransaction(() -> {
           all().forEach(user -> {
               if (user.referralCode == null || user.referralCode.isEmpty()) {
                   user.referralCode = UUID.randomUUID().toString().replace("-", "");
               }

               db.em().merge(user);
           });
        });
    }
}
