package services;

import common.Utils;
import models.Application;
import models.Branch;
import models.PageableObject;
import models.User;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by philip on 3/14/17.
 */
@Singleton
public class ApplicationService {
    @Inject
    private JPAApi db;

    @Inject
    private UserService userService;

    public Application create(Application application) {
        db.em().persist(application);
        return application;
    }

    public void save(Application application) {
        db.em().merge(application);
    }

    public Optional<Application> byId(Long applicationId) {
        return DBQuery.findOrEmpty(() ->
                db.em().createQuery("select a from Application a where a.id = ?1", Application.class)
                .setParameter(1, applicationId)
                .setMaxResults(1)
                .getSingleResult()
        );
    }

    public PageableObject allByPage(int page, ArrayList<Application.Status> status) {
        Collection<Application> apps = db.em().createQuery("select a from Application a where a.status in ?1", Application.class)
                .setParameter(1, status)
                .setFirstResult((page - 1) * Utils.pageSize())
                .setMaxResults(Utils.pageSize())
                .getResultList();

        Long count = db.em().createQuery("select count(a.id) from Application a where a.status in ?1", Long.class)
                .setParameter(1, status)
                .setMaxResults(1)
                .getSingleResult();

        PageableObject pageable = new PageableObject(apps, page, count.intValue());
        return pageable;
    }

    public PageableObject allByPageAndUser(int page, User user, ArrayList<Application.Status> status) {
        Collection<Application> apps = db.em().createQuery(
                "select a from Application a where a.status in ?1 and a.agent.id = ?2", Application.class)
                .setParameter(1, status)
                .setParameter(2, user.id)
                .setFirstResult((page - 1) * Utils.pageSize())
                .setMaxResults(Utils.pageSize())
                .getResultList();

        Long count = db.em().createQuery(
                "select count(a.id) from Application a where a.status in ?1 and a.agent.id = ?2", Long.class)
                .setParameter(1, status)
                .setParameter(2, user.id)
                .setMaxResults(1)
                .getSingleResult();

        PageableObject pageable = new PageableObject(apps, page, count.intValue());
        return pageable;
    }

    public PageableObject allByPageAndBranch(int page, Branch branch, ArrayList<Application.Status> status) {
        Collection<Long> userIds = userService.byBranch(branch);

        Collection<Application> apps = db.em().createQuery(
                "select a from Application a where a.status in ?1 and a.agent.id in ?2", Application.class)
                .setParameter(1, status)
                .setParameter(2, userIds)
                .setFirstResult((page - 1) * Utils.pageSize())
                .setMaxResults(Utils.pageSize())
                .getResultList();

        Long count = db.em().createQuery(
                "select count(a.id) from Application a where a.status in ?1 and a.agent.id in ?2", Long.class)
                .setParameter(1, status)
                .setParameter(2, userIds)
                .setMaxResults(1)
                .getSingleResult();

        PageableObject pageable = new PageableObject(apps, page, count.intValue());
        return pageable;
    }

    public PageableObject applicationsForUser(User user, int page) {
        PageableObject pageable = null;
        ArrayList<Application.Status> statusFilter = Utils.statusFilterForUser(user);

        if (user.role.equals(User.Function.ADMIN)) {
            pageable = allByPage(page, statusFilter);
        } else if (user.role.equals(User.Function.USER)) {
            pageable = allByPageAndUser(page, user, statusFilter);
        } else if (user.role.equals(User.Function.SUPERVISOR)) {
            pageable = allByPageAndBranch(page, user.branch, statusFilter);
        } else if (user.role.equals(User.Function.MANAGER)) {
            pageable = allByPage(page, statusFilter);
        }

        return pageable;
    }
}
