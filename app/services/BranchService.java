package services;

import models.Branch;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by philip on 3/16/17.
 */
@Singleton
public class BranchService {
    @Inject
    private JPAApi db;

    public Branch detach(Branch branch) {
        db.em().detach(branch);
        return branch;
    }

    public void create(Branch branch) {
        db.em().persist(branch);
    }

    public Collection<Branch> all() {
        return db.em().createQuery("select b from Branch b").getResultList();
    }

    public Optional<Branch> byId(Long id) {
        return DBQuery.findOrEmpty(() ->
            db.em().createQuery("select  b from Branch b where b.id = ?1", Branch.class)
                .setParameter(1, id)
                .setMaxResults(1)
                .getSingleResult()
        );
    }

    public Optional<Branch> byNameOrCode(String name, String code) {
        return DBQuery.findOrEmpty(() ->
            db.em().createQuery("select b from Branch b where b.name = ?1 or b.code = ?2", Branch.class)
                .setParameter(1, name)
                .setParameter(2, code)
                .setMaxResults(1)
                .getSingleResult()
        );
    }

    public Optional<Branch> byName(String name) {
        return DBQuery.findOrEmpty(() ->
            db.em().createQuery("select b from Branch b where b.name = ?1", Branch.class)
                .setParameter(1, name)
                .setMaxResults(1)
                .getSingleResult()
        );
    }

    public Optional<Branch> byCode(String code) {
        return DBQuery.findOrEmpty(() ->
                db.em().createQuery("select b from Branch b where b.code = ?1", Branch.class)
                        .setParameter(1, code)
                        .setMaxResults(1)
                        .getSingleResult()
        );
    }

    public void save(Branch branch) {
        db.em().merge(branch);
    }
}
