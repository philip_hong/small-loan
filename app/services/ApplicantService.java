package services;

import models.Applicant;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;

/**
 * Created by philip on 3/14/17.
 */
@Singleton
public class ApplicantService {
    @Inject
    private JPAApi db;

    public Optional<Applicant> byIdentificationNumberOrPhone(String idNumber, String phone) {
        return DBQuery.findOrEmpty(() ->
            db.em().createQuery("select a from Applicant a where a.identificationNumber = ?1 or a.phone = ?2", Applicant.class)
                    .setMaxResults(1)
                    .setParameter(1, idNumber)
                    .setParameter(2, phone)
                    .getSingleResult()
        );
    }
}
